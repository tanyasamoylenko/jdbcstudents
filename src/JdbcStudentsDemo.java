import java.sql.*;

/**
 * Created by Администратор on 16.11.14.
 */
public class JdbcStudentsDemo {
    static final String DB_NAME = "C:/Users/Администратор//IdeaProjects/JdbcStudents";
    static final String LOGIN = "";
    static final String PASSWORD = "";
    Connection connection;

    public JdbcStudentsDemo(){
        openConnection();
    }

    public Connection openConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql:"+DB_NAME, LOGIN, PASSWORD);
            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteTableStudents() throws SQLException {
        String sql = "DROP TABLE STUDENTS IF EXISTS";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void deleteTableMarks() throws SQLException {
        String sql = "DROP TABLE MARKS IF EXISTS";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void createTableStudents() throws SQLException {
        String sql = "CREATE TABLE STUDENTS " +
                "(id INTEGER not NULL, " +
                " name VARCHAR(255)," +
                " PRIMARY KEY ( id ))";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void createTableMarks() throws SQLException {
        String sql = "CREATE TABLE MARKS " +
                "(id INTEGER not NULL PRIMARY KEY, " +
                " mark INTEGER," +
                " student_id INTEGER," +
                " FOREIGN KEY(student_id) REFERENCES STUDENTS(id))";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void addStudent(int id, String name) throws SQLException {
        String sql = "INSERT INTO STUDENTS VALUES("+id+", '"+name+"')";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void addMark(int id, int mark, int student_id) throws SQLException {
        String sql = "INSERT INTO MARKS VALUES("+id+", "+mark+", "+student_id+")";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
    }

    public void printStudents() throws SQLException {
        String sql = "SELECT ID, NAME FROM STUDENTS ORDER BY NAME";
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while (res.next()){
            int id = res.getInt(1);
            String name = res.getString(2);
            System.out.println(id+": "+name);
        }
    }

    public void printMarks() throws SQLException {
        String sql = "SELECT ID, MARK, STUDENT_ID FROM MARKS";
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while (res.next()){
            int id = res.getInt(1);
            int mark = res.getInt(2);
            int student_id = res.getInt(3);
            System.out.println(id+": "+mark+": "+student_id);
        }
    }

    public void printStudentsMark() throws SQLException {
        String sql = "SELECT s.id, s.name, AVG(m.mark)AS mark FROM STUDENTS AS s JOIN MARKS AS m ON m.student_id=s.id GROUP BY s.id";
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while (res.next()){
            int id = res.getInt("id");
            String name = res.getString("name");
            int mark = res.getInt("mark");
            System.out.println(id+": "+name+": "+mark);
        }
    }

    public static void main(String args[]){
        JdbcStudentsDemo jdbc = null;
        try {
            jdbc = new JdbcStudentsDemo();

            jdbc.deleteTableStudents();
            //jdbc.createTableStudents();
            jdbc.addStudent(1, "Samoilenko Tanya");
            jdbc.addStudent(2, "Samoilenko Den");
            jdbc.addStudent(3, "Agafontseva Irina");
            jdbc.addStudent(4, "Kotova Julia");

            jdbc.deleteTableMarks();
            jdbc.createTableMarks();
            jdbc.addMark(1, 5, 1);
            jdbc.addMark(2, 5, 2);
            jdbc.addMark(3, 4, 3);
            jdbc.addMark(4, 4, 4);

            jdbc.addMark(5, 5, 1);
            jdbc.addMark(6, 5, 2);
            jdbc.addMark(7, 5, 3);
            jdbc.addMark(8, 5, 4);

            jdbc.addMark(9, 5, 1);
            jdbc.addMark(10, 5, 2);
            jdbc.addMark(11, 5, 3);
            jdbc.addMark(12, 5, 4);

            jdbc.printStudentsMark();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            jdbc.closeConnection();
        }
    }
}
